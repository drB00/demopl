import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, APP_INITIALIZER } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import 'hammerjs';
import { NgxSpinnerModule } from 'ngx-spinner';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSelectModule } from '@angular/material/select';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster';
import { FormsModule } from '@angular/forms';
import { NgxGalleryModule } from 'ngx-gallery';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfigService } from './shared/services/config.service';
import { HeaderComponent } from './shared/components/header/header.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { CountriesRegionsComponent } from './pages/countries-regions/countries-regions.component';
import { RulesComponent } from './shared/dialogs/rules/rules.component';
import { MoreAboutComponent } from './pages/more-about/more-about.component';
import { AllRegionsComponent } from './pages/all-regions/all-regions.component';
import { ListofComponent } from './pages/listof/listof.component';
import { SearchExtComponent } from './pages/search-ext/search-ext.component';
import { PaginationComponent } from './shared/components/pagination/pagination.component';
import { MapComponent } from './shared/dialogs/map/map.component';
import { PopupComponent } from './shared/dialogs/popup/popup.component';
import { SearchBaseComponent } from './pages/search-base/search-base.component';
import { SafeHtmlPipe } from './shared/pipes/safe-html.pipe';
import { AboutComponent } from './pages/about/about.component';
import { SourceComponent } from './pages/source/source.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    CountriesRegionsComponent,
    RulesComponent,
    MoreAboutComponent,
    AllRegionsComponent,
    ListofComponent,
    SearchExtComponent,
    MapComponent,
    PaginationComponent,
    PopupComponent,
    SearchBaseComponent,
    SafeHtmlPipe,
    AboutComponent,
    SourceComponent
  ],
  entryComponents: [RulesComponent, MapComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule,
    MatInputModule,
    BrowserAnimationsModule,
    NgxSpinnerModule,
    MatDialogModule,
    MatExpansionModule,
    MatSelectModule,
    LeafletModule.forRoot(),
    LeafletMarkerClusterModule,
    NgxGalleryModule
  ],
  providers: [
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigService],
      multi: true
    },
    { provide: LOCALE_ID, useValue: 'ru' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
export function ConfigLoader(configService: ConfigService) {
  return () => configService.load(environment.configFile);
}
