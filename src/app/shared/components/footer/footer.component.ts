import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { RulesComponent } from '../../dialogs/rules/rules.component';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  year: number;
  constructor(private dialog: MatDialog) {}

  ngOnInit() {
    let now = new Date();
    this.year = now.getFullYear();
  }

  onTop() {
    let posY = document.documentElement.scrollTop || document.body.scrollTop;
    const interval = setInterval(() => {
      posY -= 40;
      window.scroll(0, posY);
      if (posY <= 0) {
        clearInterval(interval);
      }
    });
  }

  open() {
    let dialogRef = this.dialog.open(RulesComponent, {
      panelClass: 'my-panel',
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      data: {}
    });
    dialogRef.afterClosed().subscribe(async result => {});
  }
}
