import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostListener
} from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';
import { MapComponent } from '../../dialogs/map/map.component';
import { MatDialog } from '@angular/material/dialog';
import { MainstoreService } from '../../services/mainstore.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [fadeStateTrigger],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  advanced = true;
  isCollapsed = true;

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.code === 'Enter') {
      if (
        !isNullOrUndefined(this.mainstoreService.searchFilter.searchStr) &&
        this.mainstoreService.searchFilter.searchStr !== ''
      ) {
        this.linkSearchbase();
      }
    }
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    public mainstoreService: MainstoreService,
    private router: Router,
    private dialog: MatDialog
  ) {
    router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        this.advanced = val.url === '/' || val.urlAfterRedirects === '/';
      }
    });
  }

  ngOnInit() {}

  linkSearch() {
    this.router.navigate(['/search']);
  }
  linkSearchbase() {
    this.router.navigate(['/searchbase']);
  }
  linkMain() {
    this.router.navigate(['/']);
  }

  open() {
    let dialogRef = this.dialog.open(MapComponent, {
      panelClass: 'my-map',
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      data: {}
    });
    dialogRef.afterClosed().subscribe(async result => {});
  }
}
