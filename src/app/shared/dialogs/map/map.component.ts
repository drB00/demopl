import {
  Component,
  OnInit,
  Inject,
  Renderer2,
  NgZone,
  ViewChild,
  AfterViewChecked,
  HostListener
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isNullOrUndefined } from 'util';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { latLng, tileLayer } from 'leaflet';
import { MainstoreService } from '../../services/mainstore.service';
import { Router } from '@angular/router';
import { GeodataService } from '../../services/geodata.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit, AfterViewChecked {
  height = '400px';

  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    maxZoom: 18,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  // options: any;
  // markerClusterGroup: L.MarkerClusterGroup;
  // markerClusterData: L.Marker[] = [];
  // markerClusterOptions: L.MarkerClusterGroupOptions = {
  //   singleMarkerMode: false,
  //   showCoverageOnHover: false,
  //   animateAddingMarkers: true,
  //   animate: true,
  //   iconCreateFunction: (cluster: L.MarkerCluster) => {
  //     let arr = cluster.getAllChildMarkers();
  //     let count = cluster.getChildCount();
  //     if (!isNullOrUndefined(arr)) {
  //       count = 0;
  //       arr.map(ar => {
  //         count += +ar.options.alt;
  //       });
  //     }
  //     let divIcon = new L.DivIcon({
  //       html:
  //         arr.length > 1
  //           ? '<div class="marker-cluster-custom"><span>' +
  //             count +
  //             '</span></div>'
  //           : '<div class="marker-cluster-custom"><span></span></div>'
  //     });
  //     return divIcon;
  //   }
  // };
  options: L.MapOptions;
  markerClusterGroup: L.MarkerClusterGroup;
  markerClusterGroup2: L.MarkerClusterGroup;
  markerClusterData: L.Marker[] = [];
  markerClusterData2: L.Marker[] = [];
  markerClusterOptions: L.MarkerClusterGroupOptions = {
    singleMarkerMode: false,
    showCoverageOnHover: false,
    animateAddingMarkers: true,
    animate: true,
    iconCreateFunction: (cluster: L.MarkerCluster) => {
      let arr = cluster.getAllChildMarkers();
      let count = cluster.getChildCount();
      if (!isNullOrUndefined(arr)) {
        count = 0;
        arr.map(ar => {
          count += +ar.options.alt;
        });
      }
      let divIcon = new L.DivIcon({
        html:
          arr.length > 1
            ? `<div class="marker-cluster-custom ${
                count > 999 ? 'marker-size-lg' : 'marker-size-sm'
              }"><span>` +
              count +
              '</span></div>'
            : '<div class="marker-cluster-custom marker-size-sm"><span></span></div>'
      });
      return divIcon;
    }
  };
  markerClusterOptions2: L.MarkerClusterGroupOptions = {
    singleMarkerMode: false,
    showCoverageOnHover: false,
    animateAddingMarkers: true,
    animate: true,
    iconCreateFunction: (cluster: L.MarkerCluster) => {
      let arr = cluster.getAllChildMarkers();
      let count = cluster.getChildCount();
      if (!isNullOrUndefined(arr)) {
        count = 0;
        arr.map(ar => {
          count += +ar.options.alt;
        });
      }
      let divIcon = new L.DivIcon({
        html:
          arr.length > 1
            ? `<div class="marker-cluster-custom2 ${
                count > 999 ? 'marker-size-lg' : 'marker-size-sm'
              }"><span>` +
              count +
              '</span></div>'
            : '<div class="marker-cluster-custom2 marker-size-sm"><span></span></div>'
      });
      return divIcon;
    }
  };
  g_obj: any;
  screenHeight: number;
  map;
  @ViewChild('mapelm', { static: true }) mapelm;
  @HostListener('window:resize', ['$event'])
  onResize(event?) {
    this.screenHeight = window.innerHeight;
  }
  constructor(
    private renderer: Renderer2,
    private ngZone: NgZone,
    private router: Router,
    public mainstoreService: MainstoreService,
    private geodataService: GeodataService,
    public dialogRef: MatDialogRef<MapComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.onResize();
  }

  ngAfterViewChecked() {
    if (this.mapelm) {
      let height = `${this.screenHeight - 250}px`;
      this.renderer.setStyle(this.mapelm.nativeElement, 'height', height);
    }
  }

  async ngOnInit() {
    this.markerClusterData = this.generateData(0);
    this.markerClusterData2 = this.generateData(1);
    this.markerClusterGroup = L.markerClusterGroup(this.markerClusterOptions);
    this.markerClusterGroup2 = L.markerClusterGroup(this.markerClusterOptions2);
    this.markerClusterGroup.clearLayers();
    this.markerClusterGroup2.clearLayers();
    this.markerClusterGroup.addLayers(this.markerClusterData);
    this.markerClusterGroup2.addLayers(this.markerClusterData2);
    this.options = {
      layers: [
        this.streetMaps,
        this.markerClusterGroup,
        this.markerClusterGroup2
      ],
      zoom: 5,
      zoomControl: false,
      center: latLng(0, 0)
    };
    try {
      this.g_obj = await this.geodataService.getAll();
      this.fitMap(
        this.g_obj.min_latitude,
        this.g_obj.max_latitude,
        this.g_obj.min_longitude,
        this.g_obj.max_longitude
      );
    } catch (err) {
      console.error(err);
    }
  }

  generateData(rtype): L.Marker[] {
    const data: L.Marker[] = [];
    for (let i = 0; i < this.mainstoreService.localites.length; i++) {
      if (this.mainstoreService.localites[i].rtype === rtype) {
        let icon;
        if (rtype === 0) {
          icon = L.icon({
            iconUrl: './assets/marker-icon.2273e3d8ad9264b7daa5.png'
          });
        }
        if (rtype === 1) {
          icon = L.icon({
            iconUrl: './assets/marker-icon.2273e3d8ad9264b7daa5_2.png'
          });
        }
        let attr = isNullOrUndefined(
          this.mainstoreService.localites[i].coalesce
        )
          ? '0'
          : this.mainstoreService.localites[i].coalesce;
        let m: L.Marker;
        m = L.marker(
          [
            this.mainstoreService.localites[i].latitude,
            this.mainstoreService.localites[i].longitude
          ],
          {
            icon,
            alt: attr
          }
        );
        let attrStr =
          attr == '1'
            ? `${attr} список`
            : attr == '2' || attr == '3' || attr == '4'
            ? `${attr} списка`
            : `${attr} списков`;
        let myPopup = L.popup().setContent(
          `<span id="link" style="color: #000; font-size: 14px">${this.mainstoreService.localites[i].name}</span><br><span style="color: #b1b1b1; font-size: 12px">${this.mainstoreService.localites[i].republic_name}</span><br><br><span id="linkPopup" style="color: #a6281b; cursor: pointer;">${attrStr}</span>`
        );
        m.bindPopup(myPopup, { className: 'another-popup' });
        m.on('click', () => {
          let elm = document.querySelector('#linkPopup');
          this.renderer.listen(elm, 'click', () => {
            if (!isNullOrUndefined(this.mainstoreService.localites[i])) {
              this.linkSearch(
                this.mainstoreService.localites[i].republic_id,
                this.mainstoreService.localites[i].region_id,
                this.mainstoreService.localites[i].district_id,
                this.mainstoreService.localites[i].id
              );
            }
          });
        });
        data.push(m);
      }
    }
    return data;
  }

  linkSearch(republic_id, region_id, distric_id, locality_id) {
    this.ngZone
      .run(() =>
        this.router.navigate(['/search'], {
          queryParams: { republic_id, region_id, distric_id, locality_id }
        })
      )
      .then(() => {});
    this.close();
  }

  mapReady(map: L.Map) {
    this.map = map;
    map.addControl(L.control.zoom({ position: 'topright' }));
  }

  fitMap(
    min_latitude: number,
    max_latitude: number,
    min_longitude: number,
    max_longitude: number
  ) {
    if (!isNullOrUndefined(this.map)) {
      try {
        this.map.fitBounds([
          [min_latitude, min_longitude],
          [min_latitude, max_longitude],
          [max_latitude, min_longitude],
          [max_latitude, max_longitude]
        ]);
      } catch (err) {
        console.error('Списков не найдено');
      }
    }
  }

  close() {
    this.dialogRef.close();
  }
}
