import {
  Component,
  OnInit,
  Renderer2,
  NgZone,
  ViewEncapsulation
} from '@angular/core';
import { MainstoreService } from 'src/app/shared/services/mainstore.service';
import { Router, ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalityService } from 'src/app/shared/services/locality.service';
import { ListService } from 'src/app/shared/services/list.service';
import { MapComponent } from 'src/app/shared/dialogs/map/map.component';
import { MatDialog } from '@angular/material/dialog';
import { ListfileService } from 'src/app/shared/services/listfile.service';

import * as L from 'leaflet';
import 'leaflet.markercluster';
import { latLng, tileLayer } from 'leaflet';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';
import { GeodataService } from 'src/app/shared/services/geodata.service';

@Component({
  selector: 'app-search-ext',
  templateUrl: './search-ext.component.html',
  styleUrls: ['./search-ext.component.scss'],
  animations: [fadeStateTrigger],
  encapsulation: ViewEncapsulation.None
})
export class SearchExtComponent implements OnInit {
  mapOpened = false;
  lists: any[] = [];
  /**Для пагинатора */
  total = 0;
  limit = 12;

  mapelm: L.Map;
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    maxZoom: 15,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  options: L.MapOptions;
  markerClusterGroup: L.MarkerClusterGroup;
  markerClusterGroup2: L.MarkerClusterGroup;
  markerClusterData: L.Marker[] = [];
  markerClusterData2: L.Marker[] = [];
  markerClusterOptions: L.MarkerClusterGroupOptions = {
    singleMarkerMode: false,
    showCoverageOnHover: false,
    animateAddingMarkers: true,
    animate: true,
    iconCreateFunction: (cluster: L.MarkerCluster) => {
      let arr = cluster.getAllChildMarkers();
      let count = cluster.getChildCount();
      if (!isNullOrUndefined(arr)) {
        count = 0;
        arr.map(ar => {
          count += +ar.options.alt;
        });
      }
      let divIcon = new L.DivIcon({
        html:
          arr.length > 1
            ? `<div class="marker-cluster-custom ${
                count > 999 ? 'marker-size-lg' : 'marker-size-sm'
              }"><span>` +
              count +
              '</span></div>'
            : '<div class="marker-cluster-custom marker-size-sm"><span></span></div>'
      });
      return divIcon;
    }
  };
  markerClusterOptions2: L.MarkerClusterGroupOptions = {
    singleMarkerMode: false,
    showCoverageOnHover: false,
    animateAddingMarkers: true,
    animate: true,
    iconCreateFunction: (cluster: L.MarkerCluster) => {
      let arr = cluster.getAllChildMarkers();
      let count = cluster.getChildCount();
      if (!isNullOrUndefined(arr)) {
        count = 0;
        arr.map(ar => {
          count += +ar.options.alt;
        });
      }
      let divIcon = new L.DivIcon({
        html:
          arr.length > 1
            ? `<div class="marker-cluster-custom2 ${
                count > 999 ? 'marker-size-lg' : 'marker-size-sm'
              }"><span>` +
              count +
              '</span></div>'
            : '<div class="marker-cluster-custom2 marker-size-sm"><span></span></div>'
      });
      return divIcon;
    }
  };
  calcHeight = 50;

  constructor(
    public mainstoreService: MainstoreService,
    private renderer: Renderer2,
    private ngZone: NgZone,
    private spinner: NgxSpinnerService,
    private localityService: LocalityService,
    private listService: ListService,
    private geodataService: GeodataService,
    private listfileService: ListfileService,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.activatedRoute.queryParams.subscribe(async params => {
      await this.mainstoreService.ready;
      if (!isNullOrUndefined(params.republic_id)) {
        this.mainstoreService.searchFilter.selectedRepublic_id = +params.republic_id;
        await this.selectRepublic(
          this.mainstoreService.searchFilter.selectedRepublic_id,
          false
        );
      }
      if (!isNullOrUndefined(params.region_id)) {
        this.mainstoreService.searchFilter.selectedRegion_id = +params.region_id;
        await this.selectRegion(
          this.mainstoreService.searchFilter.selectedRegion_id,
          false
        );
      }
      if (!isNullOrUndefined(params.distric_id)) {
        this.mainstoreService.searchFilter.selectedDistric_id = +params.distric_id;
        await this.selectDistric(
          this.mainstoreService.searchFilter.selectedDistric_id,
          false
        );
      }
      if (!isNullOrUndefined(params.locality_id)) {
        this.mainstoreService.searchFilter.selectedLocality_id = +params.locality_id;
        await this.selectLocality(
          this.mainstoreService.searchFilter.selectedLocality_id,
          false
        );
      }
      this.mainstoreService.searchFilter.page = 1;
      this.search();
      // setTimeout(() => {
      // }, 700);
    });
  }

  async ngOnInit() {
    this.mainstoreService.searchFilter.page = 1;
    // this.search();
  }

  generateData(rtype): L.Marker[] {
    const data: L.Marker[] = [];
    for (let i = 0; i < this.mainstoreService.localites.length; i++) {
      if (this.mainstoreService.localites[i].rtype === rtype) {
        let icon;
        if (rtype === 0) {
          icon = L.icon({
            iconUrl: './assets/marker-icon.2273e3d8ad9264b7daa5.png'
          });
        }
        if (rtype === 1) {
          icon = L.icon({
            iconUrl: './assets/marker-icon.2273e3d8ad9264b7daa5_2.png'
          });
        }
        let attr = isNullOrUndefined(
          this.mainstoreService.localites[i].coalesce
        )
          ? '0'
          : this.mainstoreService.localites[i].coalesce;
        let m: L.Marker;
        m = L.marker(
          [
            this.mainstoreService.localites[i].latitude,
            this.mainstoreService.localites[i].longitude
          ],
          {
            icon,
            alt: attr
          }
        );
        let attrStr =
          attr == '1'
            ? `${attr} список`
            : attr == '2' || attr == '3' || attr == '4'
            ? `${attr} списка`
            : `${attr} списков`;
        let myPopup = L.popup().setContent(
          `<span id="link" style="color: #000; font-size: 14px">${this.mainstoreService.localites[i].name}</span><br><span style="color: #b1b1b1; font-size: 12px">${this.mainstoreService.localites[i].republic_name}</span><br><br><span id="linkPopup" style="color: #a6281b; cursor: pointer;">${attrStr}</span>`
        );
        m.bindPopup(myPopup, { className: 'another-popup' });
        m.on('click', () => {
          let elm = document.querySelector('#linkPopup');
          this.renderer.listen(elm, 'click', () => {
            if (!isNullOrUndefined(this.mainstoreService.localites[i])) {
              this.linkSearch(
                this.mainstoreService.localites[i].republic_id,
                this.mainstoreService.localites[i].region_id,
                this.mainstoreService.localites[i].district_id,
                this.mainstoreService.localites[i].id
              );
            }
          });
        });
        data.push(m);
      }
    }
    return data;
  }

  linkSearch(republic_id, region_id, distric_id, locality_id) {
    this.ngZone
      .run(() =>
        this.router.navigate(['/search'], {
          queryParams: { republic_id, region_id, distric_id, locality_id }
        })
      )
      .then();
  }

  mapReady(map: L.Map) {
    this.mapelm = map;
    map.addControl(L.control.zoom({ position: 'topright' }));
  }

  onPan(latitude: number, longitude: number, zoom?: number) {
    setTimeout(() => {
      if (!isNullOrUndefined(this.mapelm)) {
        this.mapelm.setView(
          new L.LatLng(latitude, longitude),
          isNullOrUndefined(zoom) ? 8 : zoom
        );
        this.mapOpened = true;
      }
    }, 700);
  }

  fitMap(
    min_latitude: number,
    max_latitude: number,
    min_longitude: number,
    max_longitude: number
  ) {
    setTimeout(() => {
      if (!isNullOrUndefined(this.mapelm)) {
        try {
          this.mapelm.fitBounds([
            [min_latitude, min_longitude],
            [min_latitude, max_longitude],
            [max_latitude, min_longitude],
            [max_latitude, max_longitude]
          ]);
        } catch (err) {
          console.error('Списков не найдено');
        }
      }
    }, 700);
  }

  selectRepublic(republic_id, search = true) {
    if (isNullOrUndefined(republic_id)) {
      this.mainstoreService.searchFilter.selectedRepublic_id = null;
      this.mainstoreService.searchFilter.selectedRegion_id = null;
      this.mainstoreService.searchFilter.selectedDistric_id = null;
      this.mainstoreService.searchFilter.selectedLocality_id = null;
      this.mainstoreService.searchFilter.selectedRepublic = null;
      this.mainstoreService.searchFilter.selectedRegion = null;
      this.mainstoreService.searchFilter.selectedDistric = null;
      this.mainstoreService.searchFilter.selectedLocality = null;
      this.mainstoreService.searchFilter.page = 1;
    } else {
      if (
        isNullOrUndefined(
          this.mainstoreService.searchFilter.selectedRepublic
        ) ||
        this.mainstoreService.searchFilter.selectedRepublic.id !== republic_id
      ) {
        this.mainstoreService.searchFilter.selectedRepublic = this.mainstoreService.republics.find(
          republic => republic.id === republic_id
        );
        this.mainstoreService.searchFilter.selectedRegion_id = null;
        this.mainstoreService.searchFilter.selectedDistric_id = null;
        this.mainstoreService.searchFilter.selectedLocality_id = null;
        this.mainstoreService.searchFilter.selectedRegion = null;
        this.mainstoreService.searchFilter.selectedDistric = null;
        this.mainstoreService.searchFilter.selectedLocality = null;
        this.mainstoreService.searchFilter.page = 1;
      }
    }
    if (search === true) {
      this.search();
    }
  }
  selectRegion(region_id, search = true) {
    if (isNullOrUndefined(region_id)) {
      this.mainstoreService.searchFilter.selectedRegion_id = null;
      this.mainstoreService.searchFilter.selectedDistric_id = null;
      this.mainstoreService.searchFilter.selectedLocality_id = null;
      this.mainstoreService.searchFilter.selectedRegion = null;
      this.mainstoreService.searchFilter.selectedDistric = null;
      this.mainstoreService.searchFilter.selectedLocality = null;
      this.mainstoreService.searchFilter.page = 1;
    } else {
      if (
        isNullOrUndefined(this.mainstoreService.searchFilter.selectedRegion) ||
        this.mainstoreService.searchFilter.selectedRegion.id !== region_id
      ) {
        this.mainstoreService.searchFilter.selectedRegion = this.mainstoreService.regions.find(
          region => region.id === region_id
        );
        this.mainstoreService.searchFilter.selectedDistric_id = null;
        this.mainstoreService.searchFilter.selectedLocality_id = null;
        this.mainstoreService.searchFilter.selectedDistric = null;
        this.mainstoreService.searchFilter.selectedLocality = null;
        this.mainstoreService.searchFilter.page = 1;
      }
    }
    if (search === true) {
      this.search();
    }
  }
  async selectDistric(distric_id, search = true) {
    if (isNullOrUndefined(distric_id)) {
      this.mainstoreService.searchFilter.selectedDistric_id = null;
      this.mainstoreService.searchFilter.selectedLocality_id = null;
      this.mainstoreService.searchFilter.selectedDistric = null;
      this.mainstoreService.searchFilter.selectedLocality = null;
      this.mainstoreService.searchFilter.page = 1;
    } else {
      if (
        isNullOrUndefined(this.mainstoreService.searchFilter.selectedDistric) ||
        this.mainstoreService.searchFilter.selectedDistric.id !== distric_id
      ) {
        this.mainstoreService.searchFilter.selectedDistric = this.mainstoreService.districts.find(
          district => district.id === distric_id
        );
        try {
          this.spinner.show();
          let _localites = this.localityService.getByDistrict(distric_id);
          let localites = isNullOrUndefined(await _localites)
            ? []
            : await _localites;
          this.mainstoreService.searchFilter.selectedDistric[
            'localites'
          ] = localites;
          this.mainstoreService.searchFilter.selectedLocality = null;
          this.mainstoreService.searchFilter.page = 1;
        } catch (err) {
          console.error(err);
        } finally {
          this.spinner.hide();
        }
      }
    }
    if (search === true) {
      this.search();
    }
  }

  async selectLocality(locality_id, search = true) {
    if (isNullOrUndefined(locality_id)) {
      this.mainstoreService.searchFilter.selectedLocality_id = null;
      this.mainstoreService.searchFilter.selectedLocality = null;
      this.mainstoreService.searchFilter.page = 1;
    } else {
      if (
        isNullOrUndefined(
          this.mainstoreService.searchFilter.selectedLocality
        ) ||
        this.mainstoreService.searchFilter.selectedLocality.id !== locality_id
      ) {
        if (
          !isNullOrUndefined(
            this.mainstoreService.searchFilter.selectedDistric.localites
          )
        ) {
          this.mainstoreService.searchFilter.selectedLocality = this.mainstoreService.searchFilter.selectedDistric.localites.find(
            locality => locality.id === locality_id
          );
        } else {
          try {
            this.spinner.show();
            let locality = this.localityService.getOneById(locality_id);
            this.mainstoreService.searchFilter.selectedLocality = isNullOrUndefined(
              await locality
            )
              ? null
              : await locality;
            this.mainstoreService.searchFilter.page = 1;
            this.search();
          } catch (err) {
            console.error(err);
          } finally {
            this.spinner.hide();
          }
        }
      }
    }
    if (search === true) {
      this.search();
    }
  }

  linkList(id) {
    this.router.navigate(['/list', id]);
  }

  async search() {
    if (this.mapOpened === false) {
      await this.mainstoreService.ready;
      this.markerClusterData = this.generateData(0);
      this.markerClusterData2 = this.generateData(1);
      this.markerClusterGroup = L.markerClusterGroup(this.markerClusterOptions);
      this.markerClusterGroup2 = L.markerClusterGroup(
        this.markerClusterOptions2
      );
      this.markerClusterGroup.clearLayers();
      this.markerClusterGroup2.clearLayers();
      this.markerClusterGroup.addLayers(this.markerClusterData);
      this.markerClusterGroup2.addLayers(this.markerClusterData2);
      this.options = {
        layers: [
          this.streetMaps,
          this.markerClusterGroup,
          this.markerClusterGroup2
        ],
        zoom: 7,
        zoomControl: false,
        center: latLng(
          isNullOrUndefined(this.mainstoreService.searchFilter.selectedLocality)
            ? 55.751244
            : this.mainstoreService.searchFilter.selectedLocality.latitude,
          isNullOrUndefined(this.mainstoreService.searchFilter.selectedLocality)
            ? 37.618423
            : this.mainstoreService.searchFilter.selectedLocality.longitude
        )
      };
      this.mapOpened = true;
    }
    let lat: number;
    let long: number;
    let g_obj: any;
    if (
      !isNullOrUndefined(this.mainstoreService.searchFilter.selectedLocality)
    ) {
      lat = this.mainstoreService.searchFilter.selectedLocality.latitude;
      long = this.mainstoreService.searchFilter.selectedLocality.longitude;
      this.onPan(
        isNullOrUndefined(lat) ? 55.751244 : lat,
        isNullOrUndefined(long) ? 37.618423 : long,
        14
      );
    } else {
      if (
        !isNullOrUndefined(this.mainstoreService.searchFilter.selectedDistric)
      ) {
        g_obj = await this.geodataService.getByDistrict(
          this.mainstoreService.searchFilter.selectedDistric.id
        );
      } else if (
        !isNullOrUndefined(this.mainstoreService.searchFilter.selectedRegion)
      ) {
        g_obj = await this.geodataService.getByRegion(
          this.mainstoreService.searchFilter.selectedRegion.id
        );
      } else if (
        !isNullOrUndefined(this.mainstoreService.searchFilter.selectedRepublic)
      ) {
        g_obj = await this.geodataService.getByRepublic(
          this.mainstoreService.searchFilter.selectedRepublic.id
        );
      } else {
        g_obj = await this.geodataService.getAll();
      }
      this.fitMap(
        g_obj.min_latitude,
        g_obj.max_latitude,
        g_obj.min_longitude,
        g_obj.max_longitude
      );
    }
    // }
    let start = this.limit * (this.mainstoreService.searchFilter.page - 1);
    let str = '';
    if (
      !isNullOrUndefined(this.mainstoreService.searchFilter.selectedRepublic_id)
    ) {
      str = `republic_id=${this.mainstoreService.searchFilter.selectedRepublic_id}`;
    }
    if (
      !isNullOrUndefined(this.mainstoreService.searchFilter.selectedRegion_id)
    ) {
      str = `region_id=${this.mainstoreService.searchFilter.selectedRegion_id}`;
    }
    if (
      !isNullOrUndefined(this.mainstoreService.searchFilter.selectedDistric_id)
    ) {
      str = `district_id=${this.mainstoreService.searchFilter.selectedDistric_id}`;
    }
    if (
      !isNullOrUndefined(this.mainstoreService.searchFilter.selectedLocality_id)
    ) {
      str = `locality_id=${this.mainstoreService.searchFilter.selectedLocality_id}`;
    }
    try {
      this.spinner.show();
      let lists = this.listService.getSearch(str, start, this.limit);
      let total = this.listService.getSearch_count(str);
      this.lists = isNullOrUndefined(await lists) ? [] : await lists;
      this.lists.map(list => {
        if (
          !isNullOrUndefined(list.listfiles) &&
          !isNullOrUndefined(list.listfiles[0])
        ) {
          list['preview'] = this.listfileService.getPreviewbyId(
            list.listfiles[0].id
          );
        }
      });
      this.total = (await total).rowcount;
      this.calcHeight = Math.ceil(this.lists.length / 4) * 250;
    } catch (err) {
      console.error(err);
    } finally {
      this.spinner.hide();
    }
  }

  goToPage(n: number): void {
    this.mainstoreService.searchFilter.page = n;
    this.search();
  }

  onNext(): void {
    this.mainstoreService.searchFilter.page++;
    this.search();
  }

  onPrev(): void {
    this.mainstoreService.searchFilter.page = 1;
    this.search();
  }

  onNext10(n = 1): void {
    this.mainstoreService.searchFilter.page += 10 * n;
    this.search();
  }

  onPrev10(n = 1): void {
    this.mainstoreService.searchFilter.page -= 10 * n;
    this.search();
  }

  linkMap() {
    let dialogRef = this.dialog.open(MapComponent, {
      panelClass: 'my-map',
      maxWidth: '100vw',
      maxHeight: '100vh',
      height: '100%',
      width: '100%',
      data: {}
    });
    dialogRef.afterClosed().subscribe(async result => {});
  }
}
