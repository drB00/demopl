import { Component, OnInit } from '@angular/core';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';
import { RequestsService } from 'src/app/shared/services/requests.service';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-source',
  templateUrl: './source.component.html',
  styleUrls: ['./source.component.scss'],
  animations: [fadeStateTrigger]
})
export class SourceComponent implements OnInit {
  title = 'Источники данных';
  htmlContent = '';
  contents: any[] = [];
  constructor(private requestsService: RequestsService) {}

  async ngOnInit() {
    let contents = this.requestsService.getAll();
    this.contents = isNullOrUndefined(await contents) ? [] : await contents;
    this.htmlContent = this.contents[0].content;
  }
}
