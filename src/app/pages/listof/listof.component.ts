import { Component, OnInit, Renderer2, NgZone, ViewChild } from '@angular/core';
import { isNullOrUndefined } from 'util';
import * as L from 'leaflet';
import 'leaflet.markercluster';
import { latLng, tileLayer } from 'leaflet';

import { Router, ActivatedRoute } from '@angular/router';
import {
  NgxGalleryOptions,
  NgxGalleryImage,
  NgxGalleryComponent,
  NgxGalleryAnimation
} from 'ngx-gallery';
import { MainstoreService } from 'src/app/shared/services/mainstore.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalityService } from 'src/app/shared/services/locality.service';
import { ListlocalityService } from 'src/app/shared/services/listlocality.service';
import { ListService } from 'src/app/shared/services/list.service';
import { ListfileService } from 'src/app/shared/services/listfile.service';

@Component({
  selector: 'app-listof',
  templateUrl: './listof.component.html',
  styleUrls: ['./listof.component.scss']
})
export class ListofComponent implements OnInit {
  id: number;

  selectedList: any;
  selectedListJpgFiles = [];
  selectedListFiles = [];
  listlocalityLinks = [];
  listlocalitysId = [];
  /**Галерея */
  imgBufsize = 5; // буфер изображений в галерее
  selectedArchivefolderfilesimg = [];
  galleryOptions: NgxGalleryOptions[] = [];
  galleryImages: NgxGalleryImage[] = [];
  @ViewChild('gallary', { static: true }) gallary: NgxGalleryComponent;
  showGallary = false;
  inProgress = false;
  inProgressVal = 0;
  setImgIdx: number; // индекс картинки,открытой в галерее
  setImgId: number; // id открытая в галерее

  calcWidth = 100;

  maplatitude: number;
  maplongitude: number;

  mapOpened = false;
  mapelm: L.Map;
  streetMaps = tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    maxZoom: 14,
    attribution:
      '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  options: L.MapOptions;
  markerClusterGroup: L.MarkerClusterGroup;
  markerClusterData: L.Marker[] = [];
  markerClusterOptions: L.MarkerClusterGroupOptions = {
    singleMarkerMode: false,
    showCoverageOnHover: false,
    animateAddingMarkers: true,
    animate: true,
    iconCreateFunction: (cluster: L.MarkerCluster) => {
      let arr = cluster.getAllChildMarkers();
      let count = cluster.getChildCount();
      if (!isNullOrUndefined(arr)) {
        count = 0;
        arr.map(ar => {
          count += +ar.options.alt;
        });
      }
      let divIcon = new L.DivIcon({
        html:
          arr.length > 1
            ? '<div class="marker-cluster-custom"><span>' +
              count +
              '</span></div>'
            : '<div class="marker-cluster-custom"><span></span></div>'
      });
      return divIcon;
    }
  };

  constructor(
    public mainstoreService: MainstoreService,
    private activatedRoute: ActivatedRoute,
    private renderer: Renderer2,
    private ngZone: NgZone,
    private router: Router,
    private spinnerService: NgxSpinnerService,
    private spinner: NgxSpinnerService,
    private localityService: LocalityService,
    private listlocalityService: ListlocalityService,
    private listService: ListService,
    private listfileService: ListfileService
  ) {
    this.activatedRoute.params.subscribe(async param => {
      await this.mainstoreService.ready;
      this.id = +param.id;
      this.getData();
    });
  }

  async ngOnInit() {
    // await this.getData();
    await this.setOptions();
  }

  generateData(): L.Marker[] {
    const data: L.Marker[] = [];
    for (let i = 0; i < this.mainstoreService.localites.length; i++) {
      const icon = L.icon({
        iconUrl: './assets/marker-icon.2273e3d8ad9264b7daa5.png'
      });
      let attr = isNullOrUndefined(this.mainstoreService.localites[i].coalesce)
        ? '0'
        : this.mainstoreService.localites[i].coalesce;
      let m: L.Marker;
      m = L.marker(
        [
          this.mainstoreService.localites[i].latitude,
          this.mainstoreService.localites[i].longitude
        ],
        {
          icon,
          alt: attr
        }
      );
      let attrStr =
        attr == '1'
          ? `${attr} список`
          : attr == '2' || attr == '3' || attr == '4'
          ? `${attr} списка`
          : `${attr} списков`;
      let myPopup = L.popup().setContent(
        `<span id="link" style="color: #000; font-size: 14px">${
          this.mainstoreService.localites[i].name
        }</span><br><span style="color: #b1b1b1; font-size: 12px">РСФСР</span><br><br><span id="linkPopup" style="color: #a6281b; cursor: pointer;">${attrStr}</span>`
      );
      m.bindPopup(myPopup, { className: 'another-popup' });
      m.on('click', () => {
        let elm = document.querySelector('#linkPopup');
        this.renderer.listen(elm, 'click', () => {
          if (!isNullOrUndefined(this.mainstoreService.localites[i])) {
            this.linkSearch(
              this.mainstoreService.localites[i].republic_id,
              this.mainstoreService.localites[i].region_id,
              this.mainstoreService.localites[i].district_id,
              this.mainstoreService.localites[i].id
            );
          }
        });
      });
      data.push(m);
    }
    return data;
  }

  linkSearch(republic_id, region_id, distric_id, locality_id) {
    this.ngZone
      .run(() =>
        this.router.navigate(['/search'], {
          queryParams: { republic_id, region_id, distric_id, locality_id }
        })
      )
      .then();
  }

  mapReady(map: L.Map) {
    this.mapelm = map;
    map.addControl(L.control.zoom({ position: 'topright' }));
  }

  fitMap() {
    if (
      !isNullOrUndefined(this.mapelm) &&
      !isNullOrUndefined(this.listlocalityLinks)
    ) {
      let arr = [];
      this.listlocalityLinks.map(link => {
        arr.push([link.latitude, link.longitude]);
      });
      try {
        this.mapelm.fitBounds(arr);
      } catch (err) {
        console.error('Списков не найдено');
      }
    }
  }

  setOptions(
    arrowPrevIcon = 'fa fa-arrow-circle-o-left',
    arrowNextIcon = 'fa fa-arrow-circle-o-right'
  ) {
    this.galleryOptions = [
      {
        imageAnimation: NgxGalleryAnimation.Fade,
        arrowPrevIcon: arrowPrevIcon,
        arrowNextIcon: arrowNextIcon,
        previewZoom: true,
        previewRotate: false,
        thumbnails: false,
        image: false,
        height: '1px',
        previewZoomMax: 5
      }
    ];
  }

  async getData() {
    this.markerClusterData = this.generateData();
    this.markerClusterGroup = L.markerClusterGroup(this.markerClusterOptions);
    this.markerClusterGroup.clearLayers();
    this.markerClusterGroup.addLayers(this.markerClusterData);
    this.options = {
      layers: [this.streetMaps, this.markerClusterGroup],
      zoom: 5,
      zoomControl: false,
      center: latLng(0, 0)
    };
    this.mapOpened = true;
    try {
      this.spinner.show();
      let selectedList = this.listService.getOneById(this.id);
      let selectedListfiles = this.listfileService.getAllByParentId(this.id);
      let _listlocalitysId = this.listlocalityService.getByList(this.id);

      let selectedlistlocalitys = isNullOrUndefined(await _listlocalitysId)
        ? []
        : await _listlocalitysId;
      this.listlocalitysId = selectedlistlocalitys.map(
        item => item.locality_id
      );
      let used = {};
      this.listlocalitysId = this.listlocalitysId.filter(obj => {
        return obj in used ? 0 : (used[obj] = 1);
      });
      let listlocalityLinks = this.localityService.getByIds(
        this.listlocalitysId
      );
      this.listlocalityLinks = isNullOrUndefined(await listlocalityLinks)
        ? []
        : await listlocalityLinks;
      let minlatitude =
        this.listlocalityLinks.length === 0
          ? 0
          : this.listlocalityLinks[0].latitude;
      let minlongitude =
        this.listlocalityLinks.length === 0
          ? 0
          : this.listlocalityLinks[0].longitude;
      let maxlatitude =
        this.listlocalityLinks.length === 0
          ? 0
          : this.listlocalityLinks[0].latitude;
      let maxlongitude =
        this.listlocalityLinks.length === 0
          ? 0
          : this.listlocalityLinks[0].longitude;
      for (let i = 0; i < this.listlocalityLinks.length; i++) {
        if (this.listlocalityLinks[i].latitude > maxlatitude) {
          maxlatitude = this.listlocalityLinks[i].latitude;
        }
        if (this.listlocalityLinks[i].longitude > maxlongitude) {
          maxlongitude = this.listlocalityLinks[i].longitude;
        }
        if (this.listlocalityLinks[i].latitude < minlatitude) {
          minlatitude = this.listlocalityLinks[i].latitude;
        }
        if (this.listlocalityLinks[i].longitude < minlongitude) {
          minlongitude = this.listlocalityLinks[i].longitude;
        }
      }
      this.maplatitude = (minlatitude + maxlatitude) / 2;
      this.maplongitude = (minlongitude + maxlongitude) / 2;
      this.selectedListFiles = isNullOrUndefined(await selectedListfiles)
        ? []
        : await selectedListfiles;
      this.selectedList = isNullOrUndefined(await selectedList)
        ? []
        : await selectedList;
      this.selectedListFiles.map(
        list => (list['preview'] = this.listfileService.getPreviewbyId(list.id))
      );
      this.selectedListFiles.sort((a, b) =>
        a.listfile.filename > b.listfile.filename ? 1 : -1
      );
      this.selectedArchivefolderfilesimg = this.selectedListFiles.filter(
        selectedArchivefolderfile =>
          this.isImgtype(selectedArchivefolderfile.listfile.filename)
      );
      this.calcWidth = this.selectedListFiles.length * 380;
    } catch (err) {
      console.error(err);
    } finally {
      this.fitMap();
      this.spinner.hide();
    }
  }

  isImgtype(filename: string): boolean {
    let res = false;
    switch (
      filename
        .toLowerCase()
        .split('.')
        .reverse()[0]
    ) {
      case 'png':
      case 'jpg':
      case 'jpeg':
      case 'bmp':
        res = true;
        break;
      default:
        res = false;
        break;
    }
    return res;
  }

  // async onShowImg1(id) {
  //   let fname: string;
  //   this.selectedListFiles.map(item => {
  //     if (item.id === id) {
  //       fname = item.listfile.filename;
  //     }
  //   });
  //   if (this.galleryImages.length === 0) {
  //     this.spinner.show();
  //     this.inProgress = true;
  //     this.inProgressVal = 0;
  //     this.inProgressVal += 100 / this.selectedListFiles.length;
  //     await Promise.all(
  //       this.selectedListFiles.map(item => {
  //         return new Promise(async resolve => {
  //           let arr = item.listfile.filename.split('.');
  //           let type = arr[arr.length - 1];
  //           let resfile;
  //           try {
  //             let file = await this.listfileService.getFilePreviewById(item.id);
  //             this.inProgressVal += 100 / this.selectedListFiles.length;
  //             resfile = new Blob([file], { type: 'image/jpg' });
  //             let reader = new FileReader();
  //             reader.readAsDataURL(resfile);
  //             reader.onloadend = () => {
  //               let base64data = String(reader.result);
  //               this.galleryImages.push({
  //                 small: base64data,
  //                 medium: base64data,
  //                 big: base64data,
  //                 description: item.listfile.filename
  //               });
  //               this.galleryImages.sort((a, b) =>
  //                 a.description > b.description ? 1 : -1
  //               );
  //               resolve();
  //             };
  //           } catch (err) {
  //             resolve();
  //           }
  //         });
  //       })
  //     );
  //     this.inProgress = false;
  //     this.spinner.hide();
  //   }
  //   setTimeout(() => {
  //     this.showGallary = true;
  //     let idx = this.galleryImages.findIndex(
  //       item => item.description === fname
  //     );
  //     this.gallary.openPreview(idx);
  //   }, 300);
  // }

  async onShowImg(id) {
    let archivefolderfiles: any[];
    let parchivefolderfiles: any[];
    let narchivefolderfiles: any[];
    let idx = this.selectedArchivefolderfilesimg.findIndex(
      item => item.id === id
    );
    parchivefolderfiles = this.selectedArchivefolderfilesimg.slice(
      idx,
      this.imgBufsize + idx > this.selectedArchivefolderfilesimg.length
        ? this.selectedArchivefolderfilesimg.length
        : this.imgBufsize + idx
    );
    narchivefolderfiles = this.selectedArchivefolderfilesimg.slice(
      idx - this.imgBufsize > 0 ? idx - this.imgBufsize : 0,
      idx
    );
    archivefolderfiles = [...narchivefolderfiles, ...parchivefolderfiles];
    let fname: string;
    this.selectedArchivefolderfilesimg.map(item => {
      if (item.id === id) {
        fname = item.listfile.filename;
      }
    });
    // if (this.galleryImages.length === 0) {
    if (true) {
      this.spinnerService.show();
      this.inProgress = true;
      // this.inProgressVal += 100 / archivefolderfiles.length;
      await Promise.all(
        // this.selectedArchivefolderfiles.map(item => {
        archivefolderfiles.map(item => {
          return new Promise(async resolve => {
            let arr = item.listfile.filename.split('.');
            let type = arr[arr.length - 1];
            let resfile;
            if (this.isImgtype(type)) {
              try {
                let file = await this.listfileService.getFilePreviewById(
                  item.id
                );
                // this.inProgressVal += 100 / archivefolderfiles.length;
                resfile = new Blob([file], { type: 'image/jpg' });
                let reader = new FileReader();
                reader.readAsDataURL(resfile);
                reader.onloadend = () => {
                  let base64data = String(reader.result);
                  this.galleryImages.push({
                    small: base64data,
                    medium: base64data,
                    big: base64data,
                    description: item.listfile.filename,
                    label: item.id
                  });
                  this.galleryImages.sort((a, b) =>
                    a.description > b.description ? 1 : -1
                  );
                  resolve();
                };
              } catch (err) {
                resolve();
              }
            } else {
              resolve();
            }
          });
        })
      );
      this.spinnerService.hide();
      // this.inProgress = false;
    }
    setTimeout(() => {
      if (this.galleryImages.length > 0) {
        this.showGallary = true;
        let idx = this.galleryImages.findIndex(
          item => item.description === fname
        );
        this.setImgIdx = idx;
        this.gallary.openPreview(idx);
      }
    }, 300);
  }

  async previewChange(e) {
    if (this.setImgIdx > e.index) {
      // в какую сторону перелистывание
      let firstId = +this.galleryImages[0].label; // есть ли что подгружать (добавить обновление списка !!!)
      let idx = this.selectedArchivefolderfilesimg.findIndex(
        item => item.id === +firstId
      );
      if (idx === 0 || idx === -1) {
        this.setImgIdx = e.index;
        this.gallary.openPreview(e.index);
      } else {
        this.setOptions('', 'fa fa-arrow-circle-o-right');
        this.addPrevImg();
        this.setImgIdx = e.index + 1;
        this.gallary.openPreview(e.index + 1); // т.к. добавили в начало списка индекс изменился
      }
    } else if (this.setImgIdx < e.index) {
      let lastId = +this.galleryImages[this.galleryImages.length - 1].label;
      let idx = this.selectedArchivefolderfilesimg.findIndex(
        item => item.id === +lastId
      );
      if (idx === this.selectedArchivefolderfilesimg.length - 1 || idx === -1) {
        this.setImgIdx = e.index;
        this.gallary.openPreview(e.index);
      } else {
        this.setOptions('fa fa-arrow-circle-o-left', '');
        this.addNextImg();
        this.setImgIdx = e.index;
        this.gallary.openPreview(e.index);
      }
    }
    this.setImgId = +e.image.label; // id текущей картинки
  }

  async addPrevImg() {
    let firstId = +this.galleryImages[0].label;
    let idx = this.selectedArchivefolderfilesimg.findIndex(
      item => item.id === +firstId
    );
    if (idx === 0 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      let _file = this.listfileService.getFilePreviewById(
        this.selectedArchivefolderfilesimg[idx - 1].id
      );
      let file = await _file;
      this.spinnerService.hide();
      this.setOptions();
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        this.galleryImages.unshift({
          small: base64data,
          medium: base64data,
          big: base64data,
          description: this.selectedArchivefolderfilesimg[idx - 1].listfile
            .filename,
          label: this.selectedArchivefolderfilesimg[idx - 1].id
        });
      };
      return true;
    }
  }

  async addNextImg() {
    let lastId = +this.galleryImages[this.galleryImages.length - 1].label;
    let idx = this.selectedArchivefolderfilesimg.findIndex(
      item => item.id === +lastId
    );
    if (idx > this.selectedArchivefolderfilesimg.length - 1 || idx === -1) {
      return false;
    } else {
      this.spinnerService.show();
      let _file = this.listfileService.getFilePreviewById(
        this.selectedArchivefolderfilesimg[idx + 1].id
      );
      let file = await _file;
      this.spinnerService.hide();
      this.setOptions();
      let resfile = new Blob([file], { type: 'image/jpg' });
      let reader = new FileReader();
      reader.readAsDataURL(resfile);
      reader.onloadend = () => {
        let base64data = String(reader.result);
        this.galleryImages.push({
          small: base64data,
          medium: base64data,
          big: base64data,
          description: this.selectedArchivefolderfilesimg[idx + 1].listfile
            .filename,
          label: this.selectedArchivefolderfilesimg[idx + 1].id
        });
      };
      return true;
    }
  }
}
