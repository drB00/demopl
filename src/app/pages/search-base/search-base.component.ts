import { Component, OnInit, NgZone, HostListener } from '@angular/core';
import { MainstoreService } from 'src/app/shared/services/mainstore.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalityService } from 'src/app/shared/services/locality.service';
import { ListService } from 'src/app/shared/services/list.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { isNullOrUndefined } from 'util';
import { ListfileService } from 'src/app/shared/services/listfile.service';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';
import { RegionService } from 'src/app/shared/services/region.service';
import { DistrictService } from 'src/app/shared/services/district.service';

@Component({
  selector: 'app-search-base',
  templateUrl: './search-base.component.html',
  styleUrls: ['./search-base.component.scss'],
  animations: [fadeStateTrigger]
})
export class SearchBaseComponent implements OnInit {
  lists: any[] = [];
  /**Для пагинатора */
  total = 0;
  limit = 12;

  regions: any[];
  districts: any[];
  localites: any[];

  showToponims = false;

  rtype: number = null;

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (event.code === 'Enter') {
      this.search();
    }
  }

  constructor(
    public mainstoreService: MainstoreService,
    private spinner: NgxSpinnerService,
    private listService: ListService,
    private listfileService: ListfileService,
    private regionService: RegionService,
    private localityService: LocalityService,
    private districtService: DistrictService,
    private ngZone: NgZone,
    private router: Router
  ) {}

  ngOnInit() {
    this.search();
  }

  linkList(id) {
    this.router.navigate(['/list', id]);
  }

  async search() {
    this.showToponims = false;
    let start = this.limit * (this.mainstoreService.searchFilter.page - 1);
    try {
      this.spinner.show();
      let lists = this.listService.getSearchStr(
        this.mainstoreService.searchFilter.searchStr,
        start,
        this.limit,
        this.rtype
      );
      let regions = this.regionService.getWithFilter(
        100,
        this.mainstoreService.searchFilter.searchStr,
        this.rtype
      );
      let districts = this.districtService.getWithFilter(
        100,
        this.mainstoreService.searchFilter.searchStr,
        this.rtype
      );
      let localites = this.localityService.getWithFilter(
        100,
        this.mainstoreService.searchFilter.searchStr,
        this.rtype
      );
      let total = this.listService.getSearchStr_count(
        this.mainstoreService.searchFilter.searchStr,
        this.rtype
      );
      this.lists = isNullOrUndefined(await lists) ? [] : await lists;
      this.regions = isNullOrUndefined(await regions) ? [] : await regions;
      this.districts = isNullOrUndefined(await districts)
        ? []
        : await districts;
      this.localites = isNullOrUndefined(await localites)
        ? []
        : await localites;
      this.lists.map(list => {
        if (
          !isNullOrUndefined(list.listfiles) &&
          !isNullOrUndefined(list.listfiles[0])
        ) {
          list['preview'] = this.listfileService.getPreviewbyId(
            list.listfiles[0].id
          );
        }
      });
      this.total = (await total).rowcount;
    } catch (err) {
      console.error(err);
    } finally {
      if (
        !isNullOrUndefined(this.mainstoreService.searchFilter.searchStr) &&
        this.mainstoreService.searchFilter.searchStr !== ''
      ) {
        this.showToponims = true;
      }
      this.spinner.hide();
    }
  }

  linkSearch(republic_id, region_id, distric_id?, locality_id?) {
    this.ngZone
      .run(() =>
        this.router.navigate(['/search'], {
          queryParams: { republic_id, region_id, distric_id, locality_id }
        })
      )
      .then();
  }

  goToPage(n: number): void {
    this.mainstoreService.searchFilter.page = n;
    this.search();
  }

  onNext(): void {
    this.mainstoreService.searchFilter.page++;
    this.search();
  }

  onPrev(): void {
    this.mainstoreService.searchFilter.page = 1;
    this.search();
  }

  onNext10(n = 1): void {
    this.mainstoreService.searchFilter.page += 10 * n;
    this.search();
  }

  onPrev10(n = 1): void {
    this.mainstoreService.searchFilter.page -= 10 * n;
    this.search();
  }
}
