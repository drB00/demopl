import { Component, OnInit } from '@angular/core';
import { MainstoreService } from 'src/app/shared/services/mainstore.service';
import { Router } from '@angular/router';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-countries-regions',
  templateUrl: './countries-regions.component.html',
  styleUrls: ['./countries-regions.component.scss'],
  animations: [fadeStateTrigger]
})
export class CountriesRegionsComponent implements OnInit {
  title = 'Страны и области';
  constructor(
    public mainstoreService: MainstoreService,
    private router: Router
  ) {}

  ngOnInit() {
    this.mainstoreService.searchFilter.searchStr = '';
  }

  linkSearch(republic_id, region_id) {
    this.router.navigate(['/search'], {
      queryParams: { republic_id, region_id }
    });
  }
  linkAll(id, rtype) {
    if (!isNullOrUndefined(id)) {
      if (!isNullOrUndefined(this.mainstoreService.republics)) {
        this.mainstoreService.republics.map(republic => {
          if (!isNullOrUndefined(republic.regions)) {
            republic.regions.map(region => {
              if (region.id === +id) {
                region['isCollapsed'] = false;
              } else {
                region['isCollapsed'] = true;
              }
              if (!isNullOrUndefined(region.districs)) {
                region.districs.map(district => {
                  district['isCollapsed'] = true;
                });
              }
            });
          }
        });
      }
    }
    this.router.navigate(['/all'], { queryParams: { rtype } });
  }
}
