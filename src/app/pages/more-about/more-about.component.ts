import { Component, OnInit } from '@angular/core';
import { RequestsService } from 'src/app/shared/services/requests.service';
import { isNullOrUndefined } from 'util';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';

@Component({
  selector: 'app-more-about',
  templateUrl: './more-about.component.html',
  styleUrls: ['./more-about.component.scss'],
  animations: [fadeStateTrigger]
})
export class MoreAboutComponent implements OnInit {
  constructor() {}

  async ngOnInit() {}
}
