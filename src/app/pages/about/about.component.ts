import { Component, OnInit } from '@angular/core';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [fadeStateTrigger]
})
export class AboutComponent implements OnInit {
  title = 'О проекте';
  constructor() {}

  ngOnInit() {}
}
