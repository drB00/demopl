import { Component, OnInit } from '@angular/core';
import {
  MainstoreService,
  Republic
} from 'src/app/shared/services/mainstore.service';
import { LocalityService } from 'src/app/shared/services/locality.service';
import { isNullOrUndefined } from 'util';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router, ActivatedRoute } from '@angular/router';
import { fadeStateTrigger } from 'src/app/shared/animations/fade.animation';

@Component({
  selector: 'app-all-regions',
  templateUrl: './all-regions.component.html',
  styleUrls: ['./all-regions.component.scss'],
  animations: [fadeStateTrigger]
})
export class AllRegionsComponent implements OnInit {
  title = 'Обзор регионов';

  regionsStr = '';
  districtsStr = '';
  localitesStr = '';
  regionsCount = 0;
  districtsCount = 0;
  localitesCount = 0;

  republics: Republic[] = [];
  rtype = 0;

  constructor(
    public mainstoreService: MainstoreService,
    private spinner: NgxSpinnerService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private localityService: LocalityService
  ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.rtype = +params.rtype;
    });
  }

  async ngOnInit() {
    await this.mainstoreService.ready;
    this.republics =
      this.rtype !== 0
        ? this.mainstoreService.republicsWork
        : this.mainstoreService.republicsSteal;
    let locs = this.mainstoreService.localites.filter(
      item => item.rtype === this.rtype
    );
    this.localitesCount = locs.length;
    this.republics.map(republic => {
      if (!isNullOrUndefined(republic.regions)) {
        this.regionsCount += republic.regions.length;
        republic.regions.map(region => {
          if (!isNullOrUndefined(region.districs)) {
            this.districtsCount += region.districs.length;
          }
        });
      }
    });
    // let republicStr =
    //   this.mainstoreService.republics.length === 1
    //     ? 'республика'
    //     : this.mainstoreService.republics.length > 1 &&
    //       this.mainstoreService.republics.length < 3
    //     ? 'республики'
    //     : 'руспублик';
    this.regionsStr =
      this.regionsCount % 10 === 1
        ? 'регион'
        : this.regionsCount % 10 > 1 && this.regionsCount % 10 < 5
        ? 'региона'
        : 'регионов';
    this.districtsStr =
      this.districtsCount % 10 === 1
        ? 'район'
        : this.districtsCount % 10 > 1 && this.districtsCount % 10 < 5
        ? 'района'
        : 'районов';
    this.localitesStr =
      this.localitesCount % 10 === 1
        ? 'населенный пункт'
        : this.localitesCount % 10 > 1 && this.localitesCount % 10 < 5
        ? 'населенных пункта'
        : 'населенных пунктов';
  }

  async setLocality(distric) {
    if (isNullOrUndefined(distric.localites)) {
      try {
        this.spinner.show();
        let _localites = this.localityService.getByDistrict(distric.id);
        let localites = isNullOrUndefined(await _localites)
          ? []
          : await _localites;
        distric['localites'] = localites;
      } catch (err) {
        console.error(err);
      } finally {
        this.spinner.hide();
      }
    }
  }

  linkSearch(republic_id, region_id, distric_id, locality_id) {
    this.router.navigate(['/search'], {
      queryParams: { republic_id, region_id, distric_id, locality_id }
    });
  }
}
