import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { CountriesRegionsComponent } from './pages/countries-regions/countries-regions.component';
import { MoreAboutComponent } from './pages/more-about/more-about.component';
import { AllRegionsComponent } from './pages/all-regions/all-regions.component';
import { ListofComponent } from './pages/listof/listof.component';
import { SearchExtComponent } from './pages/search-ext/search-ext.component';
import { SearchBaseComponent } from './pages/search-base/search-base.component';
import { AboutComponent } from './pages/about/about.component';
import { SourceComponent } from './pages/source/source.component';

const routes: Routes = [
  { path: '', component: CountriesRegionsComponent },
  { path: 'more', component: MoreAboutComponent },
  { path: 'about', component: AboutComponent },
  { path: 'source', component: SourceComponent },
  { path: 'all', component: AllRegionsComponent },
  { path: 'all/:id', component: AllRegionsComponent },
  { path: 'search', component: SearchExtComponent },
  { path: 'searchbase', component: SearchBaseComponent },
  { path: 'list/:id', component: ListofComponent },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
